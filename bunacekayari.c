#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct songList
{
    int songLength;
    char albumTitle[100];
    char singerName[100];
    char songName[100];
    struct songList *left;
    struct songList *right;

} songList;

typedef struct albumList
{
    int releaseYear;
    int sarkiSayisi;

    char albumTitle[100];
    char singerName[100];
    struct songList *top_song;
    struct albumList *left;
    struct albumList *right;

} albumList;
albumList* deneme;
albumList *root = NULL;
songList *songRoot = NULL;
albumList * Find(albumList *node, char *albumTitle)
{
        if(node==NULL)
        {
                /* Element is not found */
                return NULL;
        }
        if(strcmp(albumTitle, node->albumTitle)>=0)
        {
                /* Search in the right sub tree. */
                return Find(node->right,albumTitle);
        }
        else if(strcmp(albumTitle, node->albumTitle)<=0)
        {
                /* Search in the left sub tree. */
                return Find(node->left,albumTitle);
        }
        else
        {
                /* Element Found */
                return node;
        }
}


albumList* FindMin(albumList *node)
{
    if(node==NULL)
    {

        return NULL;
    }
    if(node->left)
        return FindMin(node->left);
    else
        return node;
}
albumList* FindMax(albumList *node)
{
    if(node==NULL)
    {

        return NULL;
    }
    if(node->right)
        FindMax(node->right);
    else
        return node;
}

songList* FindMinSong(songList *node)
{
    if(node==NULL)
    {

        return NULL;
    }
    if(node->left)
        return FindMinSong(node->left);
    else
        return node;
}
songList* FindMaxSong(songList *node)
{
    if(node==NULL)
    {

        return NULL;
    }
    if(node->right)
        FindMaxSong(node->right);
    else
        return node;
}
void albumVarMi(albumList *node,char* albumTitle){

if(node==NULL)
    {

        return;

    }
    albumVarMi(node->left,albumTitle);
    if(strcmp(node->albumTitle,albumTitle)==0)
    {
        deneme=node;
        return;
    }
    albumVarMi(node->right,albumTitle);
}

albumList * addAlbum(albumList *node,char *albumTitle,char *singerName,int releaseYear)
{   deneme=NULL;
    albumVarMi(node,albumTitle);
            if(node==NULL)
        {
            albumList *temp;
            temp = (albumList *)malloc(sizeof(albumList));
            strcpy(temp->albumTitle,albumTitle);
            strcpy(temp->singerName,singerName);
            temp -> releaseYear = releaseYear;
            temp -> left = temp -> right = NULL;
            return temp;
        }
        if(!deneme){

        if(releaseYear >=node->releaseYear)
        {
            node->right = addAlbum(node->right,albumTitle,singerName,releaseYear);
        }
        else if(releaseYear <node->releaseYear)
        {
            node->left = addAlbum(node->left,albumTitle,singerName,releaseYear);
        }
        }
        else{
            printf("The album: %s already exist!",albumTitle);
        }

        return node;

    }


songList * addSong(songList *node,char *albumTitle,char *songName,int songLength)
{
    deneme=NULL;
    albumVarMi(root,albumTitle);
    if(deneme){
    if(node==NULL)
    {
        songList *temp;
        temp = (songList *)malloc(sizeof(songList));
        strcpy(temp->albumTitle,albumTitle);
        strcpy(temp->songName,songName);
        temp -> songLength = songLength;
        temp -> left = temp -> right = NULL;
        printf("%s added succesfully!",songName);
        return temp;
    }

    if(strcmp(songName,node->songName)>0)
    {
        node->right = addSong(node->right,albumTitle,songName,songLength);
    }
    else if(strcmp(songName,node->songName)<0)
    {
        node->left = addSong(node->left,albumTitle,songName,songLength);
    }
    else
    {
        printf("The song already exist!");
    }
    }
    else{
        printf("Album %s not found!",albumTitle);
    }
    return node;
}

songList * removeAlbumSong(songList *node,char* albumTitle)
{
    deneme=NULL;
    albumVarMi(root,albumTitle);
    if(deneme){
    songList *temp;
    if(node==NULL)
    {
        printf("Song not found!");
    }
    else if(strcmp(albumTitle,node->albumTitle)<0)
    {
        node->left = removeAlbumSong(node->left,albumTitle);
    }
    else if(strcmp(albumTitle,node->albumTitle)>0)
    {
        node->right = removeAlbumSong(node->right,albumTitle);
    }
    else
    {

        if(node->right && node->left)
        {

            temp = FindMinSong(node->right);
            strcpy(node -> songName , temp->songName);

            node -> right = removeAlbumSong(node->right,temp->albumTitle);
        }
        else
        {

            temp = node;
            if(node->left == NULL)
                node = node->right;
            else if(node->right == NULL)
                node = node->left;
            free(temp);

        }
    }
    return node;
    }
}


albumList * removeAlbum(albumList *node, char* albumTitle)
{

    albumList *temp;
    if(node==NULL)
    {
        printf("Element Not Found");
    }
    else if(strcmp(albumTitle,node->albumTitle)<0)
    {
        node->left = removeAlbum(node->left, albumTitle);
    }
    else if(strcmp(albumTitle,node->albumTitle)>0)
    {
        node->right = removeAlbum(node->right, albumTitle);
    }
    else
    {   root->sarkiSayisi=10;
        while(root->sarkiSayisi>0){
       songRoot= removeAlbumSong(songRoot,albumTitle);
       root->sarkiSayisi--;
    }

        if(node->right && node->left)
        {

            temp = FindMin(node->right);
            strcpy(node -> albumTitle, temp->albumTitle);

            node -> right = removeAlbum(node->right,temp->albumTitle);
        }
        else
        {

            temp = node;
            if(node->left == NULL)
                node = node->right;
            else if(node->right == NULL)
                node = node->left;
            free(temp);
            printf("%s deleted succesfully!",albumTitle);
        }
    }
    return node;

}


songList * removeSong(songList *node,char* albumTitle,char* songName)
{
    deneme=NULL;
    albumVarMi(root,albumTitle);
    if(deneme){
    songList *temp;
    if(node==NULL)
    {
        printf("%s Not Found",songName);
    }
    else if(strcmp(songName,node->songName)<0)
    {
        node->left = removeSong(node->left,albumTitle, songName);
    }
    else if(strcmp(songName,node->songName)>0)
    {
        node->right = removeSong(node->right,albumTitle, songName);
    }
    else
    {

        if(node->right && node->left)
        {

            temp = FindMinSong(node->right);
            strcpy(node -> songName , temp->songName);

            node -> right = removeSong(node->right,temp->albumTitle,temp->songName);
        }
        else
        {

            temp = node;
            if(node->left == NULL)
                node = node->right;
            else if(node->right == NULL)
                node = node->left;
            free(temp);
            printf("%s deleted succesfully!",songName);
        }
    }
    return node;
    }
}


void showAllAlbums(albumList *node)
{
    if(node==NULL)
    {
        return;
    }

    showAllAlbums(node->left);

    printf("%s\t\t%s\t\t%d",node->albumTitle,node->singerName,node->releaseYear);
    printf("\n");
    showAllAlbums(node->right);
}

void showListOfAllSongs(songList *node)
{
    if(node==NULL)
    {
        return;
    }

    showListOfAllSongs(node->left);

    printf("%s\t\t%s\t\t%d",node->albumTitle,node->songName,node->songLength);
    printf("\n");
    showListOfAllSongs(node->right);
}
void showListOfAlbumSongs(songList *node,char* albumTitle)
{
    if(node==NULL)
    {
        return;
    }

    showListOfAlbumSongs(node->left,albumTitle);
    if(strcmp(node->albumTitle,albumTitle)==0){
    printf("%s\t\t%s\t\t%d",node->albumTitle,node->songName,node->songLength);
    printf("\n");
    }
    showListOfAlbumSongs(node->right,albumTitle);
}

int showAlbumDetails(albumList *node,char* albumTitle)
{
    if(node==NULL)
    {
        return 0;
    }

    showAlbumDetails((node->left),albumTitle);
    if(strcmp(node->albumTitle,albumTitle)==0)
    {
        printf("%s\t\t%s\t\t%d\n",node->albumTitle,node->singerName,node->releaseYear);
        printf("List of Song->%s\n",albumTitle);
        if(songRoot!=NULL)
        showListOfAlbumSongs(songRoot,albumTitle);
        else{
            printf("---none---");
        }
        printf("\n");
        return 1;
    }
    else
    {
        return 0;
    }
    showAlbumDetails((node->right),albumTitle);
}




int searchSong(songList *node,char* songName)
{
    if(node==NULL)
    {
        return 0;
    }

    searchSong(node->left,songName);
    if(strcmp(node->songName,songName)==0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    searchSong(node->right,songName);
}

void showAlbumsInBetweenParticularYears(albumList *node,int lowerAlbumRlsYear,int upperAlbumRlsYear)
{
    if(node==NULL)
    {
        return;
    }

    showAlbumsInBetweenParticularYears(node->left,lowerAlbumRlsYear,upperAlbumRlsYear);
    if(node->releaseYear>=lowerAlbumRlsYear&&node->releaseYear<=upperAlbumRlsYear)
    {
        printf("%s\t\t%s\t\t%d",node->albumTitle,node->singerName,node->releaseYear);
        printf("\n");
    }
    showAlbumsInBetweenParticularYears(node->right,lowerAlbumRlsYear,upperAlbumRlsYear);
}

void showSongsWithParticularLength(songList *node,int lowerBoundary,int upperBoundary)
{
    if(node==NULL)
    {
        return;
    }

    showSongsWithParticularLength(node->left,lowerBoundary,upperBoundary);
    if(node->songLength>lowerBoundary&&node->songLength<upperBoundary)
    {
        printf("%s\t\t%s\t\t%d",node->albumTitle,node->songName,node->songLength);
        printf("\n");
    }
    showSongsWithParticularLength(node->right,lowerBoundary,upperBoundary);
}


int main()
{
    char tempAlbumName[100];
    char tempSingerName[100];
    char tempSongName[100];
    int i;
    int num;
    int lowerAlbumRlsYear;
    int upperAlbumRlsYear;
    int tempSongLength;
    int lowerSongLength;
    int upperSongLength;

    while(1)
    {
        printf("\nList Operations\n");
        printf("===============\n");
        printf("1.Add an album\n2.Remove an album\n3.Show the list of albums\n4.Show detailed information about a particular album\n");
        printf("5.Add a song to the song list of an album\n6.Remove a song to the song list of an album\n");
        printf("7.Show the list of all songs\n8.Query the albums which are/were released between particular years\n");
        printf("9.Query the songs whose length are in a particular scope\n10.Exit\n");
        printf("Enter your choice : ");
        if(scanf("%d",&i)<=0)
        {
            printf("Enter only an Integer\n");
            exit(0);
        }
        else
        {
            switch(i)
            {
            case 1:
                gets(tempAlbumName);
                printf("Enter the album name : ");
                gets(tempAlbumName);
                printf("Enter singer name : ");
                gets(tempSingerName);
                printf("Enter release year : ");
                scanf("%d",&num);

                root = addAlbum(root,tempAlbumName,tempSingerName,num);

                break;
            case 2:
                gets(tempAlbumName);
                printf("Enter the album name you want to delete : ");
                gets(tempAlbumName);
                root = removeAlbum(root,tempAlbumName);
                break;
            case 3:
                if(root!=NULL)
                {
                    printf("Album Title\t\tSinger Name\t\tRelease Year\n");
                    printf("------------------------------------------------------------\n");
                    showAllAlbums(root);
                }
                else
                {
                    printf("---none---");
                }
                break;
            case 4:
                printf("Which album do you want to get info? ");
                gets(tempAlbumName);
                gets(tempAlbumName);
                    showAlbumDetails(root,tempAlbumName);
                break;
            case 5:
                gets(tempAlbumName);
                printf("Enter album name : ");
                gets(tempAlbumName);
                printf("Enter song name : ");
                gets(tempSongName);
                printf("Enter song length : ");
                scanf("%d",&tempSongLength);

                if(tempSongLength>0)
                {
                    songRoot = addSong(songRoot,tempAlbumName,tempSongName,tempSongLength);

                }
                else
                {
                    printf("Song length can not be negative!");
                }


                break;

            case 6:
                gets(tempAlbumName);
                printf("Enter album name to delete song in it : ");
                gets(tempAlbumName);
                printf("Enter song name to delete : ");
                gets(tempSongName);
                songRoot=removeSong(songRoot,tempAlbumName,tempSongName);
                break;
            case 7:
                if(songRoot!=NULL)
                {
                    printf("Album Title\t\tSong Name\t\tSong Length\n");
                    showListOfAllSongs(songRoot);
                }
                else
                {
                    printf("---none---");
                }
                break;
            case 8:
                printf("Enter the lower boundary : ");
                scanf("%d",&lowerAlbumRlsYear);
                printf("Enter the upper boundary : ");
                scanf("%d",&upperAlbumRlsYear);
                if(lowerAlbumRlsYear>0&&upperAlbumRlsYear>0)
                {
                    if(lowerAlbumRlsYear<=upperAlbumRlsYear)
                    {
                        showAlbumsInBetweenParticularYears(root,lowerAlbumRlsYear,upperAlbumRlsYear);
                    }
                    else
                    {

                        printf("Lower boundary can not be greater than upper boundary!");
                    }
                }
                else
                {
                    printf("Boundaries can not be negative!");
                }
                break;
            case 9:
                printf("Enter the lower boundary : ");
                scanf("%d",&lowerSongLength);
                printf("Enter the upper boundary : ");
                scanf("%d",&upperSongLength);
                if(lowerSongLength>0&&upperSongLength>0)
                {
                    if(lowerSongLength<=upperSongLength)
                    {
                        showSongsWithParticularLength(songRoot,lowerSongLength,upperSongLength);
                    }
                    else
                    {

                        printf("Lower boundary can not be greater than upper boundary!");
                    }
                }
                else
                {
                    printf("Boundaries can not be negative!");
                }
                break;
            case 10:
                exit(0);
                break;

            default:
                printf("Invalid Option!");
            }
        }


    }
}
